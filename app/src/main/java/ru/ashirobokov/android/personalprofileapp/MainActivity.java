package ru.ashirobokov.android.personalprofileapp;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements Toolbar.OnMenuItemClickListener {

    Toolbar mToolBar;
    TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coordinator_container);

        CollapsingToolbarLayout collapsingToolbarLayout =
                (CollapsingToolbarLayout) findViewById(R.id.app_collapsing);

        collapsingToolbarLayout.setTitle(getString(R.string.app_name));

        mToolBar = (Toolbar) findViewById(R.id.app_toolbar);

        setSupportActionBar(mToolBar);
        mToolBar.setOnMenuItemClickListener(this);

        mTabLayout = (TabLayout) findViewById(R.id.app_tabs);
        setupTabs();

    }

    private void setupTabs() {

        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
 //       mTabLayout.setTabMode(TabLayout.MODE_FIXED);
        mTabLayout.addTab(mTabLayout.newTab().setText("КОНТАКТЫ"));
        mTabLayout.addTab(mTabLayout.newTab().setText("ТРУДОУСТРОЙСТВО"));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
     getMenuInflater().inflate(R.menu.toolbar_menu, menu);
     return true;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorite:
                Toast.makeText(this, "Favorite", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.action_search:
                Toast.makeText(this, "Search", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.action_settings:
                Toast.makeText(this, "Specify required Settings", Toast.LENGTH_LONG).show();
        }

    return true;
    }

}
